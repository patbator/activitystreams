<?php
require __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Patbator\ActivityStreams\Model\Note;
use Patbator\ActivityStreams\Model\Person;
use Patbator\ActivityStreams\Stream;


class BasicRenderTest extends TestCase
{
    protected $_json;

    public function setUp()
    {
        parent::setUp();

        $item = new Note;
        $item->id('http://example.org/note/123');
        $item->name('Our Weather Is Fine');
        $item->content('I feel that the weather is appropriate to our season and location.');
        $item->attributedTo([
            (new Person())
            ->id('http://joe.website.example/')
            ->name('Joe Smith')
        ]);
        $item->url('http://example.org/permalink/123');

        $stream = new Stream($item);

        $this->_json = $stream->render();
    }


    /** @test */
    public function typeShouldBeNote()
    {
        $this->assertContains('"type": "Note"', $this->_json);
    }


    /** @test */
    public function shouldBeAttributedToJoeSmith()
    {
        $this->assertContains('"name": "Joe Smith"', $this->_json);
    }
}



class BasicReadTest extends TestCase
{
    protected $_root;

    public function setUp()
    {
        parent::setUp();

        $this->_root = Stream::fromJson('{
    "@context": "https://www.w3.org/ns/activitystreams",
    "attributedTo": [
        {
            "name": "Joe Smith",
            "id": "http://joe.website.example/",
            "type": "Person"
        }
    ],
    "content": "I feel that the weather is appropriate to our season and location.",
    "name": "Our Weather Is Fine",
    "id": "http://example.org/note/123",
    "url": "http://example.org/permalink/123",
    "type": "Note"
}')
            ->getRoot();
    }


    /** @test */
    public function rootShouldBeOurANote()
    {
        $this->assertEquals(Note::class, get_class($this->_root));
    }


    public function rootProperties()
    {
        return [
            ["content", "I feel that the weather is appropriate to our season and location."],
            ["name",  "Our Weather Is Fine"],
            ["id",  "http://example.org/note/123"],
            ["url", "http://example.org/permalink/123"]
        ];
    }


    /**
     * @test
     * @dataProvider rootProperties
     */
    public function rootPropertiesShouldBeCorrect($name, $value)
    {
        $this->assertEquals($value, $this->_root->{$name}());
    }


    /** @test */
    public function firstAttributedToShouldBeAPerson()
    {
        $this->assertEquals(Person::class, get_class($this->_root->attributedTo()[0]));
    }


    public function joeProperties()
    {
        return [
            ["name",  "Joe Smith"],
            ["id",  "http://joe.website.example/"],
        ];
    }


    /**
     * @test
     * @dataProvider joeProperties
     */
    public function joePropertiesShouldBeCorrect($name, $value)
    {
        $this->assertEquals($value, $this->_root->attributedTo()[0]->{$name}());
    }

}
