<?php
namespace Patbator\ActivityStreams;

use Patbator\ActivityStreams\Model\Base;


class Stream
{
    protected $_root;

    public function __construct($root)
    {
        $this->_root = $root;
    }


    public function getRoot()
    {
        return $this->_root;
    }


    public function render()
    {
        return json_encode(
            array_merge(
                ['@context' => 'https://www.w3.org/ns/activitystreams'],
                $this->_root->toArray()
            ),
            JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE
        );
    }


    public static function fromJson($json)
    {
        if (null === $raw = json_decode($json, true))
            return;

        if (!array_key_exists('@context', $raw)
            || 'https://www.w3.org/ns/activitystreams' != $raw['@context'])
            return;

        unset($raw['@context']);

        $root = (new Base)->fromArrayOrNative($raw);

        return new static($root);
    }
}
