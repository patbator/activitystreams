<?php

namespace Patbator\ActivityStreams\Model;

class Activity extends BaseObject
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'actor' => null,
            'object' => null,
            'target' => null,
            'origin' => null,
            'result' => null,
            'instrument' => null,
        ]);
    }
}