<?php
namespace Patbator\ActivityStreams\Model;


class Base
{
    protected static $_factory;
    protected $_attribs = [];


    public function __call($name, $args=[])
    {
        if (!array_key_exists($name, $this->_attribs))
            throw new \RuntimeException('Unkown property ' . $name);

        if (!empty($args)) {
            $this->_attribs[$name] = $args[0];
            return $this;
        }

        return $this->_attribs[$name];
    }


    public function type()
    {
        return $this->_attribs['type']
            ? $this->_attribs['type']
            : (new \ReflectionClass($this))->getShortName();
    }


    public function toArray()
    {
        $properties = $this->_attribs;
        $properties['type'] = $this->type();
        $filtered = array_filter($properties, function($item) {
            return null !== $item;
        });

        return array_map([$this, 'toArrayOrNative'], $filtered);
    }


    public function toArrayOrNative($value)
    {
        if (is_array($value))
            return array_map([$this, 'toArrayOrNative'], $value);

        if (!is_object($value))
            return $value;

        return ((new \ReflectionClass($value))->isSubclassOf(Base::class))
            ? $value->toArray()
            : $value;
    }


    public function fromArray(array $raw)
    {
        foreach($raw as $k => $v)
            $this->{$k}($this->fromArrayOrNative($v));

        return $this;
    }


    public function fromArrayOrNative($value)
    {
        if (!is_array($value))
            return $value;

        if (array_key_exists('type', $value)) {
            $model = $this->getFactory()->newWithType($value['type']);
            unset($value['type']);
            $model->fromArray($value);

            return $model;
        }

        return array_map([$this, 'fromArrayOrNative'], $value);
    }


    public function getFactory()
    {
        return static::$_factory
            ? static::$_factory
            : new Factory();
    }


    public static function setFactory($factory)
    {
        if ($factory)
            static::$_factory = $factory;
    }
}
