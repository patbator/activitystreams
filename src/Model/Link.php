<?php

namespace Patbator\ActivityStreams\Model;

class Link extends Base
{
    public function __construct()
    {
        $this->_attribs = [
            'href' => null,
            'rel' => null,
            'mediaType' => null,
            'name' => null,
            'hreflang' => null,
            'height' => null,
            'width' => null,
            'preview' => null,
        ];
    }
}
