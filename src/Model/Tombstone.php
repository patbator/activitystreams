<?php

namespace Patbator\ActivityStreams\Model;

class Tombstone extends BaseObject
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'formerType' => null,
            'deleted' => null,
        ]);
    }
}
