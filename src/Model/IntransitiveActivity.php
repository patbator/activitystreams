<?php

namespace Patbator\ActivityStreams\Model;

class IntransitiveActivity extends Activity
{
    public function __construct()
    {
        parent::__construct();
        unset(this->_attribs['object']);
    }
}
