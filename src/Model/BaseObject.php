<?php
namespace Patbator\ActivityStreams\Model;


class BaseObject extends Base
{
    protected $_actor_attribs;
    
    public function __construct()
    {
        $this->_attribs = [
            'attachment' => null,
            'attributedTo' => null,
            'audience' => null,
            'content' => null,
            'context' => null,
            'contentMap' => null,
            'name' => null,
            'nameMap' => null,
            'endTime' => null,
            'generator' => null,
            'id' => null,
            'icon' => null,
            'image' => null,
            'inReplyTo' => null,
            'location' => null,
            'preview' => null,
            'published' => null,
            'replies' => null,
            'startTime' => null,
            'summary' => null,
            'summaryMap' => null,
            'tag' => null,
            'updated' => null,
            'url' => null,
            'to' => null,
            'type' => null,
            'bto' => null,
            'cc' => null,
            'bcc' => null,
            'mediaType' => null,
            'duration' => null,
        ];

        $this->_actor_attribs = [
            'inbox', 'outbox', 'following', 'followers', 'liked',
            'streams', 'preferredUsername', 'endpoints', 'proxyUrl',
            'oautAuthorizationEndpoint', 'oauthTokenEndpoint',
            'provideClientKey', 'signClientKey',
            'sharedInbox'
        ];
    }


    public function __call($name, $args=[])
    {
        if (
            in_array($name, $this->_actor_attribs)
            && !array_key_exists($name, $this->_attribs)
        )
            $this->asActor();

        return parent::__call($name, $args);
    }


    public function asActor()
    {
        foreach($this->_actor_attribs as $attrib)
            $this->_attribs[$attrib] = null;

        return $this;
    }
}
