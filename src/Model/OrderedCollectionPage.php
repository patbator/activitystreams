<?php

namespace Patbator\ActivityStreams\Model;

class OrderedCollectionPage extends OrderedCollection
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'partOf' => null,
            'next' => null,
            'prev' => null,
            'startIndex' => null,
        ]);
    }
}
