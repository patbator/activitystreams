<?php

namespace Patbator\ActivityStreams\Model;

class CollectionPage extends Collection
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'partOf' => null,
            'next' => null,
            'prev' => null,
        ]);
    }
}
