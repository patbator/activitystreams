<?php

namespace Patbator\ActivityStreams\Model;

class Profile extends BaseObject
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'describes' => null,
        ]);
    }
}
