<?php
namespace Patbator\ActivityStreams\Model;


class Collection extends BaseObject
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'totalItems' => null,
            'current' => null,
            'first' => null,
            'last' => null,
            'items' => null,
        ]);
    }
}
