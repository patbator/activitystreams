<?php

namespace Patbator\ActivityStreams\Model;

class Question extends IntransitiveActivity
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'oneOf' => null,
            'anyOf' => null,
            'closed' => null,
        ]);
    }
}
