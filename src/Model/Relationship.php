<?php

namespace Patbator\ActivityStreams\Model;

class Relationship extends BaseObject
{
    public function __construct()
    {
        parent::__construct();
        $this->_attribs = array_merge($this->_attribs, [
            'subject' => null,
            'object' => null,
            'relationship' => null,
        ]);
    }
}
