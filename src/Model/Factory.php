<?php
namespace Patbator\ActivityStreams\Model;


class Factory
{
    protected $_map = [];

    public function newWithType($type)
    {
        $type = array_key_exists($type, $this->_map)
            ? $this->_map[$type]
            : '\\Patbator\\ActivityStreams\\Model\\' . $type;

        return new $type();
    }


    public function mapTypeToClass($type, $class)
    {
        if (!$type || !$class)
            return $this;

        $this->_map[$type] = $class;
        return $this;
    }


    public function unmapType($type)
    {
        if (!$type)
            return $this;

        unset($this->_map[$type]);
        return $this;
    }
}