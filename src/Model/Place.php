<?php

namespace Patbator\ActivityStreams\Model;

class Place extends BaseObject
{
    public function __construct()
    {
        parent::__construct();

        $this->_attribs = array_merge($this->_attribs, [
            'accuracy' => null,
            'altitude' => null,
            'latitude' => null,
            'longitude' => null,
            'radius' => null,
            'units' => null,
        ]);
    }
}
