# ActivityStreams

> A PHP implementation of the Activity Streams 2.0 

This library aims to provide AS2 vocabulary as php objets from json and to serialize them back in json.
It does not aim to be a full JSON-LD implementation.


## Install

### With Composer

``` bash
$ composer require patbator/activitystreams
```

### Manually

Clone or download repository.

Require the provided PSR-4 autoloader to use ActivityStreams objects
``` php
require_once '[path where you cloned or downloaded this library]/autoload.php';
```

## Usage

Let start with the first https://www.w3.org/TR/activitystreams-core/ example.

### Objects to JSON

``` php
use Patbator\ActivityStreams\Model\Note;
use Patbator\ActivityStreams\Stream;

$note = new Note;
$note->content('My dog has fleas.');
$note->summary('A note');

$stream = new Stream($item);

echo $stream->render();

```

### Will output :
``` json
{
    "@context": "https://www.w3.org/ns/activitystreams",
    "content": "My dog has fleas.",
    "summary": "A note",
    "type": "Note"
}
```

### JSON to objects

``` php
use Patbator\ActivityStreams\Stream;

$note = Stream::fromJson('{
  "@context": "https://www.w3.org/ns/activitystreams",
  "summary": "A note",
  "type": "Note",
  "content": "My dog has fleas."
}')->getRoot();

echo $note->content();

```

### Will output :

```
My dog has fleas.
```

See tests for more usage samples.

## Extending

You could need to provide your own class for a type, for example to implement https://www.w3.org/wiki/SocialCG/ActivityPub/Authentication_Authorization.

Imagine you want an Service having a ```publicKey``` property.

``` php
use \Patbator\ActivityStreams\Model\Service;


class My_ActivityPub_Service extends Service 
{
  public function __construct() 
  {
    parent::__construct();
    $this->_actor_attribs[] = 'publicKey';
  }


  public function type()
  {
    return 'Service';
  }
}
```

You can declare it to type factory:

``` php
use Patbator\ActivityStreams\Model\Base;
use Patbator\ActivityStreams\Model\Factory;

Base::setFactory((new Factory)
                 ->mapTypeToClass('Service', 'My_ActivityPub_Service'));
```

After that, factory will create a new ```My_ActiviyPub_Service``` for any json object with ```"type": "Service"``` instead of a ```\\Patbator\\ActivityStreams\\Model\\Service```.

## Testing

``` bash
$ composer test
```

## License

The MIT License (MIT). Please see LICENSE.md for more information.
